# -*- coding: utf-8 -*-

import os
import tornado.web
import tornado.ioloop
import tornado.httpserver
from url import url

app = tornado.web.Application(url)

def init():
    if not os.path.exists("./cache"):
        os.mkdir("./cache")

if __name__ == '__main__':
    init()
    server = tornado.httpserver.HTTPServer(app)
    server.listen(12000)
    tornado.ioloop.IOLoop.instance().start()