# -*- coding: utf-8 -*-

import os
import time
from datetime import datetime
import tornado.web
import sys
import json
import resource

class Ctrl(tornado.web.RequestHandler):

    def check_stat(self, filename):
        if not os.path.exists(filename):
            return False
        ctime = datetime.fromtimestamp(os.path.getctime(filename))
        ntime = datetime.now()
        return (ntime - ctime).seconds < 12 * 60 * 60

    def get(self):
        self.set_header('Content-Type', 'text/xml;charset=UTF-8')
        name = self.get_argument('name')
        filename = os.path.join('./cache', '%s.xml' % name)
        if self.check_stat(filename):
            ret = None
            with open(filename) as f:
                ret = f.read()
            self.write(ret)
        else:
            ret = getattr(resource, name)()
            with open(filename, 'w') as f:
                f.write(ret)
            self.write(ret)

    def post(self):
        return self.get()
