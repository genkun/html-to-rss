# -*- coding: utf-8 -*-

import re
from datetime import datetime
from dateutil.parser import parse as dateutil_parse
import PyRSS2Gen
from requests_html import HTMLSession

HEADERS = {
    'Upgrade-Insecure-Requests':'1',
    'User-Agent':'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36',
    'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'DNT':'1',
    'Accept-Encoding':'gzip, deflate, br',
    'Accept-Language':'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7',
    'Cookie':'pr=1; tz=-480; sz=1844'
}

def ez_rss_gen(title, link, description, raw_items):
    items = []
    for it in raw_items:
        if type(it) == dict:
            item = PyRSS2Gen.RSSItem(
                title=it['title'],
                link=it['link'],
                description=it['desc'] if 'desc' in it else it['title'],
                guid=it['link'],
                pubDate=it['date']
            )
            items.append(item)
        elif type(it) == tuple:
            item = PyRSS2Gen.RSSItem(
                title=it[0],
                link=it[1],
                description=it[3] if len(it) > 3 else it[0],
                guid=it[1],
                pubDate=it[2]
            )
            items.append(item)
    rss = PyRSS2Gen.RSS2(
        title=title,
        link=link,
        description=description,
        items=items
    )
    return rss.to_xml(encoding='utf-8')

def yihui():
    url = 'https://yihui.name/cn/'
    session = HTMLSession()
    r = session.get(url)
    ps = r.html.find('p')
    items = []
    for p in ps[:50]:
        it = {}
        it['title'] = p.find('a')[0].text
        it['link'] = list(p.absolute_links)[0]
        it['date'] = dateutil_parse(p.find('span')[0].text)
        items.append(it)
    return ez_rss_gen('Yihui Xie', url, 'Yihui Xie', items)

def jamesalice():
    MONTH = {'一月': 1, '二月': 2, '三月': 3, '四月': 4, '五月': 5, '六月': 6,
             '七月': 7, '八月': 8, '九月': 9, '十月': 10, '十一月': 11, '十二月': 12}
    session = HTMLSession()
    r = session.get('https://blog.jamesalice.world/%E5%BD%92%E6%A1%A3')
    all_list = r.html.find('.clean-my-archives', first=True)
    h2s = all_list.find('h2')
    uls = all_list.find('ul')
    items = []
    for h2, ul in zip(h2s, uls):
        mon, year = tuple(h2.text.split())
        lis = ul.find('li')
        for li in lis:
            it = {}
            day, it['title'] = re.findall(r'(\d\d):\s(.+?)\s\(\d+\)', li.text)[0]
            it['link'] = list(li.absolute_links)[0]
            it['date'] = datetime(int(year), MONTH[mon], int(day))
            items.append(it)
    return ez_rss_gen('James & Alice', 'https://blog.jamesalice.world', 'James & Alice', items)

def dean():
    url = 'http://dean.xjtu.edu.cn/jxxx/zhtz.htm'
    session = HTMLSession()
    r = session.get(url)
    all_list = r.html.find('.list_main_content', first=True)
    lis = all_list.find('.list-li')
    items = []
    for li in lis:
        it = {}
        title = li.find('a', first=True).text
        if title[0] == '&':
            continue
        it['title'] = title
        it['link'] = list(li.find('a', first=True).absolute_links)[0]
        it['date'] = dateutil_parse(li.find('span', first=True).text)
        items.append(it)
    items.sort(key=lambda x: x['date'], reverse=True)
    return ez_rss_gen('教务通知', url, '教务通知', items)

def research_activity():
    url = 'http://sdcs.sysu.edu.cn/research/activity'
    session = HTMLSession()
    r = session.get(url)
    all_list = r.html.find('.full-page-list', first=True)
    lis = all_list.find('li')
    items = []
    for li in lis:
        it = {}
        it['title'] = li.find('a', first=True).text
        it['date'] = dateutil_parse(li.find('span', first=True).text)
        it['link'] = list(li.absolute_links)[0]
        items.append(it)
    return ez_rss_gen('学术活动', url, '学术活动', items)

def towardsdatascience():
    urls = [
        'https://towardsdatascience.com/machine-learning/home',
        'https://towardsdatascience.com/programming/home',
        'https://towardsdatascience.com/data-visualization/home'
    ]
    session = HTMLSession()
    items = []
    for url in urls:
        r = session.get(url, headers=HEADERS)
        titles = [title.text for title in r.html.find('h3')]
        posts = [post for post in r.html.find('a') if 'style' in post.attrs]
        links = [link.attrs['href'] for link in posts]
        dates = [dateutil_parse(dt.attrs['datetime']) for dt in r.html.find('time')]
        items += list(zip(titles, links, dates))
    items.sort(key=lambda x: x[2], reverse=True)
    return ez_rss_gen('Towards Data Science', urls[0], 'Towards Data Science', items)

def typeisbeautiful():
    url = 'https://www.typeisbeautiful.com/'
    session = HTMLSession()
    r = session.get(url)
    posts = r.html.find('.post')
    items = []
    for post in posts:
        h2 = post.find('h2', first=True)
        if h2 is None:
            continue
        title = h2.text
        link = list(post.find('h2', first=True).absolute_links)[0]
        date = dateutil_parse(post.find('abbr', first=True).attrs['title'])
        desc = post.find('.entry-content')[0].text[:100]
        items.append((title, link, date, desc))
    return ez_rss_gen('Type is Beautiful', url, 'Type is Beautiful', items)

def ymin():
    url = 'http://blog.sciencenet.cn/home.php?mod=space&uid=290937&do=blog&view=me&from=space'
    session = HTMLSession()
    r = session.get(url)
    posts = r.html.find('.bbda')
    items = []
    for post in posts:
        a = post.find('.xs2', first=True).find('a')[-1]
        title = a.text
        link = list(a.absolute_links)[0]
        date = dateutil_parse(post.find('.xg1', first=True).text)
        desc = post.find('.cl', first=True).text.split('闵应骅')[-1]
        items.append((title, link, date, desc))
    return ez_rss_gen('闵应骅的博客', url, '闵应骅的博客', items)
